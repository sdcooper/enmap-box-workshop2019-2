Day 2 (Tue 26.02.2019)
######################

9:00 Application Tutorial 2 & 3
===============================

:room: 1'231 and 1'230 (two groups)

10:30 Coffee Break
==================

in front of room 1'231

11:00 Input Processing Packages
===============================

*Andreas Rabe, HU Berlin*

11:30 Input GUI Programming
===========================

*Benjamin Jakimow, HU Berlin*

12:00 Poster Presentations
==========================

during lunch (12:20-13:40)

14:00 Programming Tutorial 1
============================


15:45 Coffee Break
==================


16:15 Programming Tutorial 2
============================

until 18:00

19:30 Dinner
============
