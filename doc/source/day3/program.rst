Day 3 (Wed 27.02.2019)
######################

09:00 Hackathon
===============

12:00 Coffee Break
==================

12:30 Documentation of Group Results
====================================

13:00 Closing Session
=====================

14:00 Evaluation + Outlook
==========================

14:15 Continue work on projects
===============================

until 16:30

